FROM centos:centos7

RUN yum update -y && \
    yum install -y epel-release && \
    yum install -y iproute python-setuptools hostname inotify-tools           yum-utils which && \
    yum -y install python-pip nginx haproxy && \
    yum clean all && \ 
    pip install supervisor

RUN rm -f /etc/haproxy/haproxy.cfg
RUN rm -f /etc/nginx/nginx.conf
RUN rm -f /etc/supervisor/conf.d/supervisord.conf

COPY config/haproxy.cfg /etc/haproxy/haproxy.cfg
#COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/supervisord.conf /etc/supervisor/supervisord.conf

EXPOSE 80

CMD ["/usr/bin/supervisord"]
